﻿using devMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace devMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(string txtList, string txtSearch, string typeSH)
        {
            ViewBag.txtList = txtList;
            ViewBag.txtSearch = txtSearch;
            ViewBag.typeSH = typeSH;
            List<string> convertList = new List<string>();
            SearchListModel model = new SearchListModel();
            if (!string.IsNullOrEmpty(txtList))
            {
                convertList = txtList.Split(',').ToList();
                model.Value.Add(string.Format("List : [{0}]", txtList));
                model.Value.Add(string.Format("Search : {0}", txtSearch));
                model.Value.Add("Result ::::");
                if (typeSH.Equals("1"))
                {
                    foreach (var item in convertList.Select((value, i) => (value, i)))
                    {
                        if (item.value.Equals(txtSearch.ToString()))
                        {
                            model.Value.Add(string.Format("Round {2} =====> {0} = {1} found!", txtSearch, item.value, item.i + 1));
                        }
                        else
                        {
                            model.Value.Add(string.Format("Round {2} =====> {0} != {1} ", txtSearch, item.value, item.i + 1));
                        }
                    }

                }
                else if (typeSH.Equals("2"))
                {
                    int index = convertList.BinarySearch(txtSearch.ToString());
                    if (index != -1)
                    {
                        model.Value.Add(string.Format("Round 1 =====> {0} = {1}", convertList[index], convertList[index]));
                    }
                    else
                    {
                        model.Value.Add("found!");
                    }
                }
                else if (typeSH.Equals("3"))
                {
                    var duble = convertList.Where(x => x.Contains(txtSearch.ToString())).Count();
                    if (duble > 0)
                    {
                        model.Value.Add(string.Format("Round 1 =====> {0} is duble", txtSearch));
                    }
                    else
                    {
                        model.Value.Add("found!");
                    }
                }
            }

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
