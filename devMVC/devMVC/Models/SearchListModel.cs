﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace devMVC.Models
{
    public class SearchListModel
    {
        public IList<string> Value { get; private set; }
        public SearchListModel()
        {
            Value = new List<string>();
        }
    }
}
